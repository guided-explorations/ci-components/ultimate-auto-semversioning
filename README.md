# Ultimate Automated SemVersioning - Next Version Determination with GitVersion

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/ci-components/ultimate-auto-semversioning)

## Usage

### Documentation

[Overall GitVersion Website](https://gitversion.net)

GitVersion:
1. Solves a [dizzying array of artfact / release versioning problems](https://gitversion.net/docs/learn/why) for busy repositories...
2. Through [intelligent default behaviors](https://gitversion.net/docs/reference/version-increments)...
3. That analyze your git metadata (branch names, tags, etc)...
3. But that can be overridden by [extensive configuration](https://gitversion.net/docs/reference/configuration).

### Shifting Software Supply Chain Security Left with Immutable Versions

Immutable versioning is where a version number is never used twice - which also means that once a
release is versioned, the contents never change.
The lack of easy and unique version number assignment can inhibit immutable versioning if it 
results in versions only being applied to nearly ready to ship code.
Cheap and easy to assign versions may improve your Software Supply Chain Security by allowing 
very early (left) version number assignment for any release that might become a production candidate.

### Minimum config autocreated

If you do not have a GitVersion.yml, the minimum contents below are auto-created for you:

```yaml
mode: Mainline
```

### Use GitLab Release and release-cli

GitVersion works very well with gitlab releases because releases will tag the commit (and without requiring a token) and then GitVersion uses that information on the next round to determine versions. It still works without it - but together they work very well. See either of the working examples below to see them working together.

### Including

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: $CI_SERVER_FQDN/guided-explorations/ci-components/ultimate-auto-semversioning/ultimate-auto-semversioning@<VERSION>
```
See "Components" tab in CI Catalog to copy exactly point to the current version.

where `<VERSION>` is the latest released tag or `main`.

### Using the generated semver in the pipeline

After the job runs, the following versions variables will be availble for the remainder of your pipeline to use in any job:

  - GitVersion_LegacySemVer
  - GitVersion_SemVer
  - GitVersion_FullSemVer
  - GitVersion_Major
  - GitVersion_Minor
  - GitVersion_Patch
  - GitVersion_MajorMinorPatch

This might look like magic, but it just uses [GitLab CI dotenv artifact files](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv) in [these lines](https://gitlab.com/guided-explorations/ci-components/ultimate-auto-semversioning/-/blob/main/templates/ultimate-auto-semversioning.yml?ref_type=heads#L44-58).

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| GitVersion.yml | `mode: Mainline``   | A file in your repository | A file in your repository with GitVersion defaults |
| `GITVERSION_DISABLED` | `null` (enabled) | CI Variable | Set to 'true' to disable component. Generally used when this component is embedded in another component but you want to disable it. |
| `CI_COMPONENT_TRACE` | `null` (off) | CI Variable | Set to 'true' to see more verbose component output. GitLabs standard CI_DEBUG_TRACE=true also triggers this trace. |
| `CI_GITVERSION_TRACE` | `null` (off) | CI Variable | Set to 'true' to see only verbose output of how GitVersoin determined your version number. |

### Outputs

Pipeline Variables. See [GitVersion Variables Doc Page](https://gitversion.net/docs/reference/variables) or component job log for exactly what these are like.

  - GitVersion_LegacySemVer
  - GitVersion_SemVer
  - GitVersion_FullSemVer
  - GitVersion_Major
  - GitVersion_Minor
  - GitVersion_Patch
  - GitVersion_MajorMinorPatch

### Validation Test

If you simply include the component and every job after it will have the following pipeline variables that used the infamous GitVersion to create a unique SemVer according
to complex uniqueness rules that are configurable by you in the GitVersion.yml file.

### Working Example Code Using This Component

- [Utterly Automated Software and Artifact Versioning with GitVersion](https://gitlab.com/guided-explorations/devops-patterns/utterly-automated-versioning)
- [Packaging Firmware Images - OpenWrt for RPI5](https://gitlab.com/guided-explorations/embedded/packaging/openwrt-rpi5)

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components